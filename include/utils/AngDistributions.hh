#ifndef SOURCE_ANG_DISTRIBUTION_HH
#define SOURCE_ANG_DISTRIBUTION_HH

namespace utils {
    double f(const double &x, const double &z);

    double df(const double &x);

    double nextCosSquaredTheta();
}

#endif
