#ifndef RUNACTION_HH
#define RUNACTION_HH

#include <G4Accumulable.hh>
#include <G4ParticleDefinition.hh>
#include <G4Run.hh>
#include <G4UserRunAction.hh>

class RunAction : public G4UserRunAction {
public:
    // Constructor
    RunAction();

    // Destructor
    ~RunAction();

    // Main interface
    void BeginOfRunAction(const G4Run *run);

    void EndOfRunAction(const G4Run *run);

    void AddSecondary(const G4ParticleDefinition *particle_definition, G4double energy);

    void OnFullEnergyEvent();

    void OnEvent();

private:
    G4Accumulable<G4int> n_full_energy_events;
    G4Accumulable<G4int> n_events;

    G4Accumulable<G4int> n_gammas;
    G4Accumulable<G4double> avg_gamma_energy;
};

#endif
