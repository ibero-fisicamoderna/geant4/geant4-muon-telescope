#ifndef EVENT_ACTION_HH
#define EVENT_ACTION_HH

#include <G4UserEventAction.hh>
#include <globals.hh>

/// Event action class
/// Provides access to the G4Event* information at the begin and
/// at the end of each event
class RunAction;

class EventAction : public G4UserEventAction {
public:
    // Constructor
    EventAction(RunAction *run_action);

    // Destructor
    ~EventAction();

    // User actions to be performed at the beginning of each event
    void BeginOfEventAction(const G4Event *event) override;

    // User actions to be performed at the end of each event
    void EndOfEventAction(const G4Event *event) override;

private:
    RunAction *run_action;
    int collision_id = -1;
};

#endif
