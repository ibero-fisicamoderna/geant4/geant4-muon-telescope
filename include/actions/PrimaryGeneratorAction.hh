#ifndef PRIMARY_GENERATOR_ACTION_HH
#define PRIMARY_GENERATOR_ACTION_HH

#include "G4VUserPrimaryGeneratorAction.hh"

// FOWARD DECLARATIONS
class G4GeneralParticleSource;

/// The primary generator action class
class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {
public:
    // Constructor
    PrimaryGeneratorAction();

    // Destructor
    ~PrimaryGeneratorAction();

    // Main interface
    void GeneratePrimaries(G4Event *event) override;

private:
    // G4GeneralParticleSource* gps;
    G4GeneralParticleSource *gps;

    void GeneratePrimary(G4Event *event);
};

#endif
