#ifndef ACTION_INITIALIZATION_HH
#define ACTION_INITIALIZATION_HH

#include <G4VUserActionInitialization.hh>

// Action initialization class.
class ActionInitialization : public G4VUserActionInitialization {
public:
    // Constructor
    ActionInitialization();

    // Destructor
    ~ActionInitialization();

    // Register User Actions for the workers
    void Build() const override;

    // Registers User Actions for the master (only Run Action)
    void BuildForMaster() const override;
};

#endif
