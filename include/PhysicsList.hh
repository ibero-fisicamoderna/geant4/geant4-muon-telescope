#ifndef PHYSICS_LIST_HH
#define PHYSICS_LIST_HH

#include <G4VModularPhysicsList.hh>

// Modular physics list
class PhysicsList : public G4VModularPhysicsList {
public:
    // Constructor
    PhysicsList();

    // Destructor
    virtual ~PhysicsList();

    // Builds particles
    void ConstructParticle() override;

    // Build processes
    void ConstructProcess() override;

    // Set user cuts
    void SetCuts() override;
};

#endif
