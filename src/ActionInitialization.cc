#include "actions/PrimaryGeneratorAction.hh"
#include "actions/EventAction.hh"
#include "actions/RunAction.hh"
#include "actions/StackingAction.hh"

#include "ActionInitialization.hh"

//! Class constructor
ActionInitialization::ActionInitialization() :
        G4VUserActionInitialization() {
    // Do nothing
}

//! Class destructor
ActionInitialization::~ActionInitialization() {
    // Do nothing
}

void ActionInitialization::Build() const {
    auto *run_action = new RunAction();

    SetUserAction(run_action);
    SetUserAction(new PrimaryGeneratorAction());
    SetUserAction(new StackingAction(run_action));
    SetUserAction(new EventAction(run_action));
}

void ActionInitialization::BuildForMaster() const {
    // By default, don't do anything. Required only for
    // specific MT applications
}
