#include <vector>
#include <cstdlib>
#include <ctime>

#include <G4RunManager.hh>
#include <G4VisExecutive.hh>
#include <G4UIExecutive.hh>
#include <G4UImanager.hh>

#include "ActionInitialization.hh"
#include "DetectorConstruction.hh"
#include "PhysicsList.hh"

using namespace std;

/* Main function that enables to:
 * - run any number of macros (put them as command-line arguments)
 * - start interactive UI mode (no arguments or "-i")
 */

int main(int argc, char **argv) {
    // INIT RAND SEED
    srand(time(nullptr));

    // RUN
    vector<string> macros;
    bool interactive = false;

    // Parse command line arguments
    if (argc == 1) {
        interactive = true;
    } else {
        for (int i = 1; i < argc; i++) {
            string arg = argv[i];

            if (arg == "-i" || arg == "--interactive") {
                interactive = true;
            } else {
                macros.push_back(arg);
            }
        }
    }

    //Use sequential mode only
    G4RunManager run_manager;
    run_manager.SetVerboseLevel(0);

    G4VisExecutive vis_manager;
    vis_manager.SetVerboseLevel(0);
    vis_manager.Initialize();

    run_manager.SetUserInitialization(new PhysicsList());
    run_manager.SetUserInitialization(new DetectorConstruction());
    run_manager.SetUserInitialization(new ActionInitialization());

    if (interactive) {
        G4UIExecutive ui_executive(argc, argv);
        G4UImanager *ui_manager = G4UImanager::GetUIpointer();

        for (const auto &macro : macros) {
            ui_manager->ApplyCommand("/control/execute " + macro);
        }

        ui_manager->ApplyCommand("/control/execute macros/vis.mac");
        ui_manager->ApplyCommand("/control/execute macros/gps.mac");

        ui_executive.SessionStart();
    }

    return 0;
}
