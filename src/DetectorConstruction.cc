#include <G4Box.hh>
#include <G4AssemblyVolume.hh>
#include <G4LogicalVolume.hh>
#include <G4LogicalBorderSurface.hh>
#include <G4MultiFunctionalDetector.hh>
#include <G4NistManager.hh>
#include <G4OpticalSurface.hh>
#include <G4PSEnergyDeposit.hh>
#include <G4PVPlacement.hh>
#include <G4RotationMatrix.hh>
#include <G4SDManager.hh>
#include <G4SubtractionSolid.hh>
#include <G4SystemOfUnits.hh>
#include <G4ThreeVector.hh>
#include <G4Transform3D.hh>
#include <G4Tubs.hh>
#include <G4VisAttributes.hh>

#include "DetectorConstruction.hh"

DetectorConstruction::DetectorConstruction() :
        G4VUserDetectorConstruction() {
    // Do nothing
}

DetectorConstruction::~DetectorConstruction() {
    // Do nothing
}


G4VPhysicalVolume *DetectorConstruction::Construct() {
    // You have to implement this and return the pointer to the
    // physical world volume
    G4NistManager *nist = G4NistManager::Instance();
    G4double world_size_x = 2 * m;
    G4double world_size_y = 2 * m;
    G4double world_size_z = 2 * m;

    // CREATE WORLD BOX
    G4VSolid *world_box = new G4Box(
            "world",
            world_size_x / 2,
            world_size_y / 2,
            world_size_z / 2
    );

    // CREATE WORLD LOGICAL VOLUME
    G4Material *world_material = nist->FindOrBuildMaterial("G4_AIR");

    G4LogicalVolume *world_lv = new G4LogicalVolume(
            world_box,
            world_material,
            "world"
    );

    // CREATE/SET WORLD VISUTAL ATTRIBUTES
    G4VisAttributes *world_vattr = new G4VisAttributes();
    world_vattr->SetVisibility(false);
    world_lv->SetVisAttributes(world_vattr);

    // CREATE WORLD PHYSICAL VOLUME
    G4VPhysicalVolume *world_pv = new G4PVPlacement(
            nullptr,
            {}, // THIS IS EQUIVALENT TO (0,0,0)
            world_lv,
            "world",
            nullptr,
            false,
            0
    );

    // WORLD CONSTANTS
    G4double min_z = -world_size_z / 2;

    // DETECTOR GEO
    G4double stand_bar_thickness = 2 * cm;
    G4double stand_int_size_x = 50 * cm;
    G4double stand_int_size_y = 50 * cm;
    G4double stand_size_z = 100 * cm;

    G4double scintillator_size_x = 30 * cm;
    G4double scintillator_size_y = 25 * cm;
    G4double scintillator_size_z = 1.5 * cm;

    G4double al_wrapper_thickness = 160 * micrometer;
    G4double detector_solid_size_x = scintillator_size_x + al_wrapper_thickness * 2;
    G4double detector_solid_size_y = scintillator_size_y + al_wrapper_thickness * 2;
    G4double detector_solid_size_z = scintillator_size_z + al_wrapper_thickness * 2;

    // CREATE A SOLID THAT REPRESENTS THE STAND THAT WILL HOLD OUR SCINTILATORS
    G4double stand_size_x = stand_int_size_x + 2 * stand_bar_thickness;
    G4double stand_size_y = stand_int_size_y + 2 * stand_bar_thickness;
    G4double stand_int_size_z = stand_size_z - 2 * stand_bar_thickness;

    G4VSolid *stand_block_solid = new G4Box(
            "stand_block",
            stand_size_x / 2,
            stand_size_y / 2,
            stand_size_z / 2
    );

    G4VSolid *stand_internal_x_solid = new G4Box(
            "stand_internal_x",
            (stand_size_x + 1 * mm) / 2,
            stand_int_size_y / 2,
            stand_int_size_z / 2
    );

    G4VSolid *stand_internal_y_solid = new G4Box(
            "stand_internal_y",
            stand_int_size_x / 2,
            (stand_size_y + 1 * mm) / 2,
            stand_int_size_z / 2
    );

    G4VSolid *stand_internal_z_solid = new G4Box(
            "stand_internal_y",
            stand_int_size_x / 2,
            stand_int_size_y / 2,
            (stand_size_z + 1 * mm) / 2
    );

    G4VSolid *stand_t1_solid = new G4SubtractionSolid(
            "stant_t1",
            stand_block_solid,
            stand_internal_x_solid,
            0,
            {0, 0, 0}
    );

    G4VSolid *stand_t2_solid = new G4SubtractionSolid(
            "stant_t2",
            stand_t1_solid,
            stand_internal_y_solid,
            0,
            {0, 0, 0}
    );

    G4VSolid *stand_solid = new G4SubtractionSolid(
            "stand",
            stand_t2_solid,
            stand_internal_z_solid,
            0,
            {0, 0, 0}
    );

    // CREATE A LOGICAL VOLUME FOR OUR AL STAND
    G4LogicalVolume *stand_lv = new G4LogicalVolume(
            stand_solid,
            nist->FindOrBuildMaterial("G4_Al"),
            "stand"
    );

    G4VisAttributes *stand_vis = new G4VisAttributes(G4Colour::Yellow());
    stand_vis->SetVisibility(true);
    stand_vis->SetForceSolid(true);
    stand_lv->SetVisAttributes(stand_vis);

    // CREATE A SOLID THAT REPRESENTS OUR SCINTILLATOR'S GEOMETRY
    G4VSolid *scintillator_solid = new G4Box(
            "scintillator",
            scintillator_size_x / 2,
            scintillator_size_y / 2,
            scintillator_size_z / 2
    );

    // CREATE A SOLID THAT REPRESENTS OUR WRAPPERS'S GEOMETRY
    G4VSolid *al_box_solid = new G4Box(
            "al_box",
            detector_solid_size_x / 2,
            detector_solid_size_y / 2,
            detector_solid_size_z / 2
    );

    G4VSolid *wrapper_solid = new G4SubtractionSolid(
            "wrapper",
            al_box_solid,
            scintillator_solid,
            0,
            {0, 0, 0}
    );

    // CREATE A LOGICAL VOLUME FOR OUR WRAPPER
    G4LogicalVolume *wrapper_lv = new G4LogicalVolume(
            wrapper_solid,
            nist->FindOrBuildMaterial("G4_Al"),
            "wrapper"
    );

    G4VisAttributes *wrapper_vis = new G4VisAttributes(G4Colour::Blue());
    wrapper_vis->SetVisibility(true);
    wrapper_vis->SetForceSolid(false);
    wrapper_lv->SetVisAttributes(wrapper_vis);

    // CREATE A LOGICAL VOLUME FOR OUR SCINTILLATOR
    G4Material *scintillator_material = nist->FindOrBuildMaterial("G4_SODIUM_IODIDE");

    G4LogicalVolume *scintillator_lv = new G4LogicalVolume(
            scintillator_solid,
            scintillator_material,
            "scintillator"
    );

    G4VisAttributes *red = new G4VisAttributes(G4Colour::Red());
    red->SetVisibility(true);
    red->SetForceSolid(true);
    scintillator_lv->SetVisAttributes(red);

    // LETS BUILD THE DETECTOR BY PLACING
    // THE WRAPPER AND THE SCINTILLATOR INSIDE
    // THE DETECTOR ASSEMBLY VOLUME
    //
    // THIS WILL PACKAGE OUR COMPONENT INTO ONE ASSEMBLY VOLUME
    G4AssemblyVolume *detector_av = new G4AssemblyVolume();

    G4RotationMatrix ra;
    G4ThreeVector ta;
    G4Transform3D ta3d(ra, ta);

    detector_av->AddPlacedVolume(
            wrapper_lv,
            ta3d
    );

    detector_av->AddPlacedVolume(
            scintillator_lv,
            ta3d
    );

    // LETS BUILD THE STAND
    // BY PLACING THE STAND AND THE DETECTORS INSIDE IT
    // WE WILL ASSEMBLE THEM IN ORDER TO EASE FUTURE
    // OPERATIONS WITH IT AS A WHOLE
    G4AssemblyVolume *stand_av = new G4AssemblyVolume();

    G4RotationMatrix rs;
    G4ThreeVector ts;

    G4Transform3D ts3d(rs, ts);

    stand_av->AddPlacedVolume(
            stand_lv,
            ts3d
    );

    int n_scintillators = 4;
    G4double separation = 20 * cm;

    for (int i = 0; i < n_scintillators; i++) {
        G4RotationMatrix rc;
        G4ThreeVector tc;

        // PLACE EACH SCINTILLATIR HIGHER THAN THE LAST ONE
        tc.setZ(-stand_size_z / 2 + detector_solid_size_z / 2 + i * (detector_solid_size_z + separation));
        G4Transform3D tc3d(rc, tc);

        // PLACE IT IN THE STAND ASSEMBLY VOLUME
        stand_av->AddPlacedAssembly(
                detector_av,
                tc3d
        );
    }

    // LETS PLACE OUR STAND IN THE WORLD!
    G4RotationMatrix rfs;
    G4ThreeVector tfs;

    tfs.setZ(min_z + stand_size_z / 2);
    G4Transform3D tfs3d(rfs, tfs);

    stand_av->MakeImprint(
            world_lv,
            tfs3d
    );

    return world_pv;
}

// Implement the following only if you have fields / sensitive detector
void DetectorConstruction::ConstructSDandField() {
    G4SDManager *sd_manager = G4SDManager::GetSDMpointer();
    sd_manager->SetVerboseLevel(0);

    G4MultiFunctionalDetector *detector = new G4MultiFunctionalDetector("detector");
    G4VPrimitiveScorer *scorer = new G4PSEnergyDeposit("EnergyDeposit");
    detector->RegisterPrimitive(scorer);

    SetSensitiveDetector("scintillator", detector);
    sd_manager->AddNewDetector(detector);
}

