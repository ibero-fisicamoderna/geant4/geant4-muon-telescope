#include <cstdlib>

#include <G4Event.hh>
#include <G4Gamma.hh>
#include <G4GeneralParticleSource.hh>
#include <G4ParticleTable.hh>
#include <G4SystemOfUnits.hh>

#include "actions/PrimaryGeneratorAction.hh"
#include "utils/AngDistributions.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction() :
        G4VUserPrimaryGeneratorAction() {
    gps = new G4GeneralParticleSource();

    G4ParticleDefinition *particle;
    particle = G4ParticleTable::GetParticleTable()->FindParticle("mu-");

    // G4ParticleDefinition* particle = G4Gamma::Definition();
    gps->SetParticleDefinition(particle);

    // Our position will be a point because we want to specify the angles
    gps->GetCurrentSource()->GetPosDist()->SetPosDisType("Point");

    // We will assume all particles are mono energetic
    gps->GetCurrentSource()->GetEneDist()->SetEnergyDisType("Mono");
    gps->GetCurrentSource()->GetEneDist()->SetMonoEnergy(22 * MeV);

    // We will focus are beam at the center of the bottom scintillator
    gps->GetCurrentSource()->GetAngDist()->SetAngDistType("focused");
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
    delete gps;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event *event) {
    // const unsigned int N = (unsigned int) (((double) rand()) / RAND_MAX) * 9 + 1;

    // for (unsigned int i = 0; i < 10; i++) {
    //     GeneratePrimary(event);
    // }

    GeneratePrimary(event);
}

void PrimaryGeneratorAction::GeneratePrimary(G4Event *event) {
    double R = 50. * cm;

    double r_a = (((double) rand()) / RAND_MAX) * R;
    double theta_a = (((double) rand()) / RAND_MAX) * 2 * M_PI;

    double x0 = cos(theta_a) * r_a;
    double y0 = sin(theta_a) * r_a;
    double z0 = -100 * cm;

    // SAFE THETA
    double theta = 0;

    while (theta == 0) {
        theta = utils::nextCosSquaredTheta();
    }

    // RANDOM PHI
    double phi = (((double) rand()) / RAND_MAX) * 2 * M_PI;

    G4double z_plane = 200. * cm;
    G4double r = z_plane / cos(theta);

    G4double x = x0 + r * sin(theta) * cos(phi);
    G4double y = y0 + r * sin(theta) * sin(phi);
    G4double z = z0 + z_plane;

    gps->GetCurrentSource()->GetPosDist()->SetCentreCoords({x, y, z});
    gps->GetCurrentSource()->GetAngDist()->SetFocusPoint({x0, y0, z0});
    gps->GeneratePrimaryVertex(event);
}
