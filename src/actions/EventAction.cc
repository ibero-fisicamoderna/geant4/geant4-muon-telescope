#include <G4SDManager.hh>
#include <G4THitsMap.hh>
#include <G4SystemOfUnits.hh>
#include <G4Event.hh>
#include <g4root.hh>

#include "actions/EventAction.hh"
#include "actions/RunAction.hh"

EventAction::EventAction(RunAction *_run_action) :
        G4UserEventAction(),
        run_action(_run_action) {
    // Do nothing
}

EventAction::~EventAction() {
    // Do nothing
}

void EventAction::BeginOfEventAction(const G4Event * /*event*/) {
    // Do nothing
}

void EventAction::EndOfEventAction(const G4Event *event) {
    G4SDManager *sdm = G4SDManager::GetSDMpointer();
    G4AnalysisManager *analysis = G4AnalysisManager::Instance();

    // Retrieve the collectionID corresponding to hits in the NaI
    // The variable collision_id should be initialized to -1 in EventAction.hh) so  that
    // this block of code is executed only at the end of the first event.
    if (collision_id < 0) {
        collision_id = sdm->GetCollectionID("detector/EnergyDeposit");
    }

    // Hits collections
    // Get all hits-collections available for this events:
    G4HCofThisEvent *hce = event->GetHCofThisEvent();

    if (!hce) {
        return;
    }

    // ok, let's start the game: retrieve the hits-collection in the NaI.
    // This comes from a Geant4 multiscorer of type "G4PSEnergyDeposit", which scores
    // energy deposit.
    G4THitsMap<G4double> *evtMap = dynamic_cast<G4THitsMap<G4double> *>(
            hce->GetHC(collision_id)
    );

    // Store the total energy in a variable
    G4double total_energy_dep = 0.;

    //Sum all individual energy deposits in this event
    for (auto pair : *(evtMap->GetMap())) {
        G4double energy_dep = *(pair.second);
        //Sum the energy deposited in all crystals, irrespectively of threshold.
        total_energy_dep += energy_dep;
    }

    if (total_energy_dep > (661.5 * keV)) {
        run_action->OnFullEnergyEvent();
    }

    if (total_energy_dep > 0) {
        run_action->OnEvent();

        G4double energy = total_energy_dep / keV;
        analysis->FillNtupleDColumn(1, 0, energy);
        analysis->AddNtupleRow(1);

        G4cout << "The total energy deposited in this event is: " << total_energy_dep / keV << " keV " << G4endl;
    }
}
