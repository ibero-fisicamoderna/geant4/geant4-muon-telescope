#include <G4AccumulableManager.hh>
#include <G4Electron.hh>
#include <G4Gamma.hh>
#include <G4SystemOfUnits.hh>
#include <g4root.hh>

#include "actions/RunAction.hh"

RunAction::RunAction() :
        G4UserRunAction(),
        n_full_energy_events("NFEEvents", 0),
        n_events("NEvents", 0),
        n_gammas("NGammas", 0),
        avg_gamma_energy("AvgGammaEnergy", 0.) {
    G4AccumulableManager *accumulable_manager = G4AccumulableManager::Instance();

    accumulable_manager->RegisterAccumulable(n_full_energy_events);
    accumulable_manager->RegisterAccumulable(n_events);

    accumulable_manager->RegisterAccumulable(n_gammas);
    accumulable_manager->RegisterAccumulable(avg_gamma_energy);

    G4AnalysisManager *analysis_manager = G4AnalysisManager::Instance();
    analysis_manager->SetVerboseLevel(1);
    analysis_manager->SetFirstNtupleId(1);

    analysis_manager->CreateNtuple("eDep", "eDep");
    analysis_manager->CreateNtupleDColumn("eDep");
    analysis_manager->FinishNtuple();

    analysis_manager->OpenFile("task6");
}

void RunAction::BeginOfRunAction(const G4Run *) {
    G4AccumulableManager *accumulable_manager = G4AccumulableManager::Instance();
    accumulable_manager->Reset();
}

void RunAction::EndOfRunAction(const G4Run *run) {
    G4int tn_events = run->GetNumberOfEvent();

    if (tn_events == 0) {
        return;
    }

    G4AccumulableManager *accumulable_manager = G4AccumulableManager::Instance();
    accumulable_manager->Merge();

    if (IsMaster()) {
        G4cout
                << "\n--------------------End of Global Run-----------------------"
                << " \n The run was " << tn_events << " events " << G4endl;

        if (n_gammas.GetValue()) {
            G4cout
                    << " * Produced " << n_gammas.GetValue() / ((G4double) n_events.GetValue())
                    << " secondary gammas/event. Average energy: "
                    << avg_gamma_energy.GetValue() / keV / n_gammas.GetValue()
                    << " keV"
                    << G4endl;
        }

        if (n_full_energy_events.GetValue()) {
            G4cout
                    << "Full Energy Events: "
                    << n_full_energy_events.GetValue()
                    << " Total Events in Detector: "
                    << n_events.GetValue()
                    << G4endl;
        }
    } else {
        G4cout << " * No secondary gammas produced" << G4endl;
    }
}

RunAction::~RunAction() {
    G4AnalysisManager *man = G4AnalysisManager::Instance();
    man->Write();
}

void RunAction::AddSecondary(const G4ParticleDefinition *particle, G4double energy) {
    if (particle == G4Gamma::Definition()) {
        n_gammas += 1;
        avg_gamma_energy += energy;
    }
}

void RunAction::OnFullEnergyEvent() {
    n_full_energy_events += 1;
}

void RunAction::OnEvent() {
    n_events += 1;
}
