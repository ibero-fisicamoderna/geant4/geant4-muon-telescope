#include "actions/StackingAction.hh"
#include "actions/RunAction.hh"

StackingAction::StackingAction(RunAction *_run_action) :
        G4UserStackingAction(),
        run_action(_run_action) {
    // Do nothing
}

StackingAction::~StackingAction() {
    // Do nothing
}

G4ClassificationOfNewTrack StackingAction::ClassifyNewTrack(const G4Track *track) {
    if (track->GetParentID() > 0) {
        run_action->AddSecondary(
                track->GetParticleDefinition(),
                track->GetKineticEnergy()
        );
    }

    return G4UserStackingAction::ClassifyNewTrack(track);
}
